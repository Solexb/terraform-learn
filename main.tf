provider "aws" {
    region = "eu-west-3"
    access_key = "AKIAZ4BB2CNOYUQQQEEV"
    secret_key = "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"
}

variable "cidr_blocks" {
    description = "cidr block for vpc and subnets"
    type = list(string)
}

variable "environment" {
    description = "Deployment environment"
}

resource "aws_vpc" "development-vpc" {
    cidr_block = var.cidr_blocks[0]
    tags = {
      "Name" = var.environment
    }
}

resource "aws_subnet" "dev-subnet-1" {
    vpc_id = aws_vpc.development-vpc.id
    cidr_block = var.cidr_blocks[1]
    availability_zone = "eu-west-3a"
    tags = {
        Name: "dev-subnet-1"
    }
}

output "dev_aws_vpc" {
    value = aws_vpc.development-vpc.id
}

output "dev_aws_subnet" {
    value = aws_subnet.dev-subnet-1.id
}